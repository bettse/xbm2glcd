#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <error.h> //linux?
#include <sys/errno.h> //osx?

unsigned char *xbm_bits;
char         *xbm_name;
int           xbm_w, xbm_h;

void
parse_xbm( FILE * xbm )
{
  char          line[1024];
  char         *p, *e;
  int           start = 0;
  int           bptr = 0;
  int           eol;

  while ( fgets( line, 1024, xbm ) )
  {
    if ( !start )
    {
      if ( strncmp( line, "#define", 7 ) == 0 )
      {
	p = strstr( line, "width" );
	if ( p )
	  xbm_w = atoi( p + 6 );
	p = strstr( line, "height" );
	if ( p )
	  xbm_h = atoi( p + 7 );
      }
      p = line;
      if ( strncmp( line, "static char", 11 ) == 0 )
      {
	p = strstr( line, "_bits" );
	if ( p )
	{
	  *p++ = 0;
	  xbm_name = strdup( line + 12 );
	  xbm_bits = malloc( ( xbm_w + 7 ) / 8 * xbm_h );
	}
      }
      if ( ( p = strchr( p, '{' ) ) != NULL )
      {
	p++;
	start++;
      }
    }
    else
    {
      if ( ( p = strchr( line, '}' ) ) != NULL )
	*p = 0;
      p = line;
    }
    if ( start )
    {
      while ( *p )
      {
	e = p;
	while ( *e && (*e != ',' && *e != '\n' ) )
	  e++;
	if ( *e )
	{
	  eol = 0;
	  *e = 0;
	}
	else
	  eol = 1;
	while ( *p == ' ' )
	  p++;
	if( p != e )
  	  xbm_bits[bptr++] = strtol( p, NULL, 0 );
	p = e;
	if ( !eol )
	  p++;
      }
    }
  }
  fclose( xbm );
}

int
main( int argc, char *argv[] )
{
  FILE         *xbm;
  int           i, j, k, l;
  int           r;
  int           wb, hb;

  if ( argc < 2 )
  {
    fprintf( stderr, "Usage %s xbm_file\n", argv[0] );
    exit( 0 );
  }
  xbm = fopen( argv[1], "r" );
  if ( xbm == NULL )
  {
    perror( argv[1] );
    exit( 0 );
  }
  parse_xbm( xbm );

  printf( "#include <avr/pgmspace.h>\n");
  printf( "static const unsigned char %s [] PROGMEM = {\n", xbm_name );
  printf( "// first row defines - FONTWIDTH, FONTHEIGHT, ASCII START CHAR, TOTAL CHARACTERS, FONT MAP WIDTH HIGH, FONT MAP WIDTH LOW (2,56 meaning 256)\n");
  printf( "%i,%i,%i,%i,%i,%i,\n", 48, 48, 48, 11, 4, 40);

  wb = ( xbm_w + 7 ) / 8;
  hb = ( xbm_h + 7 ) / 8;
  for ( i = 0; i < hb; i++ )
  {
    for ( j = 0; j < wb; j++ )
    {
      for ( k = 0; k < 8; k++ )
      {
	for ( r = 0, l = 0; l < 8; l++ )
	{
	  r <<= 1;
	  if ( ( i != hb - 1 ) || ( 7 - l < xbm_h - ( hb - 1 ) * 8 ) )
	    r |= ( xbm_bits[wb * ( 8 * i + 7 - l ) + j] >> k ) & 1;
	}
	if ( ( j != wb - 1 ) || ( k < xbm_w - ( wb - 1 ) * 8 ) )
	  printf( "0x%02x, ", r );
      }
      printf( "\n" );
    }
  }
  printf( "};\n" );
  return 0;
}
