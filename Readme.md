## LCD Fonts from pngs (osx/linux)

This could certainly be automated or scripted, but I wanted, at the very least, to have documented the general idea of what I did.  I wanted to create a pseudo font for the MicroView so that I could display full screen icons.  I learned a lot in the process and when I was successful I wanted to record some of my experience.

### Build convert-xbm

I found this via google, host here http://uglyduck.ath.cx/graphic-lcd/convert-xbm.c

`gcc convert-xbm.c -o convert-xbm`

### Trim, resize, center

`mkdir tmp; ls -1 src/*.png | while read filename; do convert -trim -resize 48x48 -gravity center -extent 48x48 $filename ${filename/src/tmp}; done`

### Append all the images into a single file and convert to xbm format

xbm is a C byte array, but not the same as the glcd byte array

`convert tmp/*.png +append font.xbm`

### Convert from xbm into the glcd byte array format

`./convert-xbm font.xbm > font.h`

To understand how these differ, I found this post to be incredibly enlightening: http://forum.arduino.cc/index.php?topic=68580.msg510771#msg510771

> I guess my description may have been a bit misleading/wrong. The GLCD format isn't really top to bottom
and then left to right. It is left to right but 8 pixels high at a time.
(The 8 pixels are top to bottom)

> The dilemma is that both formats store pixel data from left to right but not by the same amount in height.
The XBM format moves from left to the right of the image 1 pixel height at a time while
the glcd format moves from left to right with a 8 pixel vertical "page" height at time.
glcd format does not paint top to bottom of the full image 1 pixel wide and then go left to right.
glcd paints an 8 pixel tall path from left to right.
So each byte in XBM is 8 pixels in a 1 pixel tall row until you hit the right edge of the image, while with GLCD
each byte is 8 vertical pixels tall until you hight the right edge.

> So what happens if you attempt to interpret the XBM format as glcd format,
is that the second byte of data will draw 8 vertical pixels just the right of the first 8 pixels rather than below
the first 8 pixels. So the image isn't just rotated, it is all scrambled.


### Hand modify the file to add/fix the header line

Something like the following, see http://learn.microview.io/font/creating-fonts-for-microview.html for details.


```
static const unsigned char font12x24 [] PROGMEM = {
// first row defines - FONTWIDTH, FONTHEIGHT, ASCII START CHAR, TOTAL CHARACTERS, FONT MAP WIDTH HIGH, FONT MAP WIDTH LOW (2,56 meaning 256)
    12,24,48,10,1,20,
```
